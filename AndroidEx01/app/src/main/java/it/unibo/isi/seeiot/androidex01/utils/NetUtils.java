package it.unibo.isi.seeiot.androidex01.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import it.unibo.isi.seeiot.androidex01.kb.C;

public class NetUtils {

    public static String doHttpGet(final String url) throws IOException {

        final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

        String result = "";

        try {
            Scanner s = new Scanner(new BufferedInputStream(connection.getInputStream())).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";

            Log.d(C.LOG_TAG,"GET REQUEST: " + url + " RESPONSE : " + result);
        } finally {
            connection.disconnect();
        }

        return result;
    }
}

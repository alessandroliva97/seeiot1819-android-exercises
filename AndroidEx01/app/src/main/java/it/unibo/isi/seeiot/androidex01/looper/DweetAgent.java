package it.unibo.isi.seeiot.androidex01.looper;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.io.IOException;

import it.unibo.isi.seeiot.androidex01.kb.C;
import it.unibo.isi.seeiot.androidex01.utils.NetUtils;

public class DweetAgent extends Thread {

    private Handler messageHandler;

    @SuppressLint("HandlerLeak")
    public void run() {
        Looper.prepare();

        messageHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case C.dweet.NEW_LOCATION_DETECTED_MESSAGE:
                        try {
                            NetUtils.doHttpGet(String.format("%s%s?latitude=%s&longitude=%s",
                                    C.dweet.FOR_PREFIX,
                                    C.dweet.COLLECTION_RESOURCE_NAME,
                                    msg.getData().getDouble(C.dweet.NEW_LOCATION_DETECTED_MESSAGE_LATITUDE_KEY),
                                    msg.getData().getDouble(C.dweet.NEW_LOCATION_DETECTED_MESSAGE_LONGITUDE_KEY)));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        };

        Looper.loop();
    }

    public Handler getMessageHandler(){
        return messageHandler;
    }

}

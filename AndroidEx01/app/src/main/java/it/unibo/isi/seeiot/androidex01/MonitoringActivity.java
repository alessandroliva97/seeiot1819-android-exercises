package it.unibo.isi.seeiot.androidex01;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import it.unibo.isi.seeiot.androidex01.kb.C;
import it.unibo.isi.seeiot.androidex01.looper.DweetAgent;

public class MonitoringActivity extends AppCompatActivity {

    private LocationManager lm;
    private LocationMonitor locationMonitor;

    private DweetAgent dweetAgent;

    private boolean monitoring_active = false;

    final int ACCESS_FINE_LOCATION_REQUEST = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring);

        initUI();

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationMonitor = new LocationMonitor();

        dweetAgent = new DweetAgent();
        dweetAgent.start();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationMonitor);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_REQUEST);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        lm.removeUpdates(locationMonitor);
    }

    @Override
    public void onRequestPermissionsResult(final int reqCode, final String[] permissions, final int[] res) {
        switch (reqCode) {
            case ACCESS_FINE_LOCATION_REQUEST:
                if (res.length > 0 && res[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission Granted!
                    Log.d(C.LOG_TAG, "Location Permission Granted!");
                } else {
                    //Permission denied!
                    Log.d(C.LOG_TAG, "Location Permission Denied!");
                }
            break;
        }
    }

    private void requestLocationUpdates(){

    }

    private void initUI() {
        final Button startMonitoringButton = findViewById(R.id.sensors_start_button);
        final Button stopMonitoringButton = findViewById(R.id.sensors_stop_button);

        startMonitoringButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monitoring_active = true;
                startMonitoringButton.setEnabled(false);
                stopMonitoringButton.setEnabled(true);
            }
        });


        stopMonitoringButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monitoring_active = false;
                startMonitoringButton.setEnabled(true);
                stopMonitoringButton.setEnabled(false);
            }
        });
    }

    class LocationMonitor implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            ((TextView) findViewById(R.id.latitude_label)).setText(String.format("[latitude] %s", latitude));
            ((TextView) findViewById(R.id.longitude_label)).setText(String.format("[longitude] %s", longitude));

            if(monitoring_active){
                Bundle bundle = new Bundle();
                bundle.putDouble(C.dweet.NEW_LOCATION_DETECTED_MESSAGE_LATITUDE_KEY, latitude);
                bundle.putDouble(C.dweet.NEW_LOCATION_DETECTED_MESSAGE_LONGITUDE_KEY, longitude);

                Message msg = new Message();
                msg.setData(bundle);
                msg.what = C.dweet.NEW_LOCATION_DETECTED_MESSAGE;

                dweetAgent.getMessageHandler().sendMessage(msg);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }

        @Override
        public void onProviderEnabled(String provider) { }

        @Override
        public void onProviderDisabled(String provider) { }
    }
}

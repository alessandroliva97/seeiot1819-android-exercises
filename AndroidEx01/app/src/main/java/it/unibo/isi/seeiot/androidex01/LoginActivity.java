package it.unibo.isi.seeiot.androidex01;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import it.unibo.isi.seeiot.androidex01.kb.C;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
    }

    private void initUI(){
        Button loginBtn = findViewById(R.id.login_button);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usernameField = findViewById(R.id.username_edit);
                EditText passwordField = findViewById(R.id.password_edit);

                String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();

                if(checkLogin(username, password)){
                    Intent i = new Intent(getApplicationContext(), MonitoringActivity.class);
                    startActivity(i);
                } else {
                    showAlert("Attenzione!", "Login errato. Riprovare!");
                }
            }
        });
    }

    private boolean checkLogin(final String username, final String password){
        if(username.equals(C.login.USERNAME) && password.equals(C.login.PASSWORD)){
            return true;
        }

        Log.d(C.LOG_TAG, "Login Failed!");
        return false;
    }

    private void showAlert(final String title, final String message){
        AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(false)
            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            })
            .create();

        dialog.show();
    }
}

package it.unibo.isi.seeiot.taskexample.agents;

import android.widget.TextView;

public class AgentWithWrongThreadException extends AbstractAgent {
    public TextView label;

    public AgentWithWrongThreadException(final TextView label){
        this.label = label;
    }

    @Override
    void updateCounter(int value) {
        label.setText("" + value);
    }
}
